'use strict';

var express = require('express');
var http = require('http');
var ejs = require('ejs');
var optimist = require('optimist');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var app = express();

//Todo environment
var env = process.env.NODE_ENV || 'development';

app.use(express.static(__dirname + '/public'));
app.set('port', '3000');
app.set('view engine', 'ejs');
app.set('views', 'app/view/');	
app.use(morgan('dev'));
app.use(bodyParser());
app.use(methodOverride());


app.listen(app.get('port'), function (err){
	if(err) { 
		console.log('Error creating server');
	} else {
		console.log('Server is running on', app.get('port'));
	}
});

require('./app/routes/')(app);

Eze-find DickSmith Project
---
The application will run on 46" portrait screen which display products/items.

Framework used:
---
* Ionic framework
* AngularJS
* Node.js 


Steps on running the Application
---
* git clone <repo>
* cd eze-find
* npm install 
* npm install -g grunt-cli
* grunt
* open the browser and visit http://localhost:3000

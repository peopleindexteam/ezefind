'use strict';

var ProductFactory = function($http) {
	var ProductFactory= {};
	ProductFactory.favorites = [];
	ProductFactory.compare = [];

	ProductFactory.getFavorites = function (){
		return ProductFactory.favorites;
	};

	ProductFactory.addFavorites = function (data){
		ProductFactory.favorites.push(data);
	};

	ProductFactory.getCompare = function (){
		return ProductFactory.compare;
	};

	ProductFactory.addCompare = function (data){
		if(ProductFactory.compare.length === 3){
			return;
		}
		ProductFactory.compare.push(data);
	};

	ProductFactory.getItem = function (id, callback) {
		$http.get('phones/' + id + '.json').success(function(data) {
			callback(data);
		});
	};

	ProductFactory.getPhones = function (callback){
		$http.get('phones/phones.json').success(function(data) {
			callback(data);
		});
	};

	ProductFactory.getBroadband = function (callback){
		$http.get('phones/broadband.json').success(function(data) {
			callback(data);
		});
	};

	ProductFactory.getKits = function (callback){
		$http.get('phones/kits.json').success(function(data) {
			callback(data);
		});
	};

	return ProductFactory;
};

Application.Services.factory('ProductFactory', ['$http', ProductFactory]);
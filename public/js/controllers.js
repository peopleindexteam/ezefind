'use strict';

var AppCtrl = function($scope, $ionicModal, $animate) {
  $ionicModal.fromTemplateUrl('templates/tpl/modal-search.html', function(modal) {
    $scope.modal = modal;
  }, {
    animation: 'slide-in-up',
    focusFirstInput: true
  });
    $scope.doTheBack = function(){
       window.history.back();
    };
};

var onesearchCtrl = function($scope, ProductFactory) {
    $scope.orderProp = 'age';
    $scope.phones = [];
    var show = false;
    
    ProductFactory.getPhones(function (data){
        $scope.phones = data;
    });

    $scope.listview = function(){
        show = true;
    };

    $scope.gridview = function(){
        show = false;
    };

    $scope.showView = function(){
        return show;
    };

    $scope.addCompare = function (id) {
        ProductFactory.getItem(id, function(data){
            ProductFactory.addCompare(data);
        });
    };
};

var mobileCtrl = function($scope, ProductFactory) {
    $scope.phones = [];
    $scope.orderProp = 'name';
    ProductFactory.getPhones(function (data){
        $scope.phones = data;
    });
};

var broadbandCtrl = function($scope, ProductFactory) {
  $scope.phones = [];
  var show = false;
  ProductFactory.getBroadband(function (data){
     $scope.phones = data;
  });
  
  $scope.orderProp = 'name';

   $scope.listview = function(){
     show = true;
   };

   $scope.gridview = function(){
     show = false;
   }; 

   $scope.showView = function(){
     return show;
   };
};      

var kitsCtrl = function($scope,  ProductFactory) {
  $scope.phones = [];
  $scope.header = 'Sim Starter Kits',
  $scope.orderProp = 'name';
  var show = false;
  
  ProductFactory.getKits(function (data){
     $scope.phones = data;
  });

   $scope.listview = function(){
     show = true;
   };

   $scope.gridview = function(){
     show = false;
   }; 

   $scope.showView = function(){
     return show;
   };
};

var PhoneDetailCtrl = function($scope, $stateParams, ProductFactory) {
    $scope.phone = {};
    var id = $stateParams.phoneId || '';
    
    $scope.addFavorite = function (data){
      ProductFactory.addFavorites(data);
      console.log('Added to Favorites');
    };

    $scope.addCompare = function (data){
      ProductFactory.addCompare(data);
      console.log('Added to Compare');
    };
  
    ProductFactory.getItem(id, function (data){
      $scope.phone = data; 
    });
};

var FavoritesCtrl = function($scope, ProductFactory){
    $scope.favorites = ProductFactory.getFavorites() || []; 
    $scope.removeFavoriteItem = function (id){
        $scope.favorites.splice(id, 1);
    };
};

var CompareCtrl = function($scope, ProductFactory){
    $scope.compareItem = ProductFactory.getCompare() || []; 
    // $scope.addCompare = function (data){
    //     ProductFactory.addCompare(data);
    // };    
    $scope.removeCompareItem = function (id){
        $scope.compareItem.splice(id, 1);
    };
};

var tabsController = function($scope){
    $scope.tabs = [
    {
        name: 'Mobile',
        url: 'templates/tpl/mobile-home.html',
        active1: true
    },{
        name: 'Broadband',
        url: 'templates/tpl/broadband-home.html',
        active1: false
    },{
        name: 'Sim Starter Kit',
        url: 'templates/tpl/kits-home.html',
        active1: false
    },{
        name: 'Recharge',
        url: 'templates/tpl/popular.html',
        active1: false
    }
    ];

    $scope.tab = 'templates/tpl/mobile-home.html'; /*default tab*/
    $scope.current = 'Mobile'; /*default active tab*/
    
    $scope.toggleTab = function(s){
        $scope.tab = s.url;  /*tab changed*/
        $scope.current = s.name; /* changing value of current*/
    };
};

var sharemodalCtrl = function($scope, $ionicModal){
    $ionicModal.fromTemplateUrl('templates/tpl/modal-share.html', function($ionicModal) {
        $scope.modal = $ionicModal;
        $scope.$on('$destroy', function() {
        $scope.modal.remove();
  });
    }, {
        scope: $scope,
        animation: 'fade-in'
    });  
};

var additemCtrl = function($scope, $ionicModal){
    $ionicModal.fromTemplateUrl('templates/tpl/modal-additem.html', function($ionicModal) {
        $scope.modal = $ionicModal;
        $scope.$on('$destroy', function() {
        $scope.modal.remove();
  });
    }, {
        scope: $scope,
        animation: 'slide-in-up'
    });  
};

var sidenavCtrl = function($scope, $ionicSideMenuDelegate) {
    $scope.titles = [
    {name: 'Title', id: 1}
  ];

    $scope.attendees = [
    { firstname: 'Entry', lastname: '1' },
    { firstname: 'Entry', lastname: '2' },
    { firstname: 'Entry', lastname: '3' },
    { firstname: 'Entry', lastname: '4' }
  ];

    $scope.shopbycateg = function(){
        $ionicSideMenuDelegate.toggleRight();
    };

    $scope.filter = function(){
        $ionicSideMenuDelegate.toggleRight();
    };

    $scope.sort = function(){
        $ionicSideMenuDelegate.toggleRight();
    };
};

var MainCtrl = function($scope,$location) {
    $scope.hideSidemenuBackButton = true;
    var topLevelCategories;

    topLevelCategories = $scope.categories = [
      {id: 1, name: 'Categories', taxons: [
        {id: 4, name: 'Mobile', taxons: [] },
        {id: 5, name: 'Broadband', taxons: []},
        {id: 6, name: 'SIM Starter Kits', taxons: []},
        {id: 7, name: 'Recharge', taxons: []},
      ], is_first_level: true}
    ];
  
    var getByParentId = function(id) {
      for (var i in topLevelCategories) {
        if (topLevelCategories[i].id === id) {
          return topLevelCategories[i].taxons;
        }
      }
    };

    $scope.toggleCategories = function() {
        $scope.sideMenuController.toggleLeft();
    };

    $scope.showSubcategories = function(category) {
        $scope.categories = getByParentId(category.id);
        $scope.hideSidemenuBackButton = false;
    };

    $scope.showTopLevelCategories = function () {
        $scope.categories = topLevelCategories;
        $scope.hideSidemenuBackButton = false;
    };
    $scope.redirect = function(id){
        if (parseInt(id) === 4){
            $location.path('/app/mobile');  
        }
        if(parseInt(id) === 5){
            $location.path('/app/broadband');
        }
        if(parseInt(id) === 6){
            $location.path('app/starter-kits');
        }
        if(parseInt(id) === 7){
            $location.path('app/recharge');
        }
    };
};

Application.Controllers.controller('AppCtrl', ['$scope', '$ionicModal', '$animate', AppCtrl]);
Application.Controllers.controller('onesearchCtrl', ['$scope', 'ProductFactory', onesearchCtrl]);
Application.Controllers.controller('mobileCtrl', ['$scope', 'ProductFactory', mobileCtrl]);
Application.Controllers.controller('broadbandCtrl', ['$scope', 'ProductFactory', broadbandCtrl]);
Application.Controllers.controller('kitsCtrl', ['$scope',  'ProductFactory', kitsCtrl]);
Application.Controllers.controller('PhoneDetailCtrl', ['$scope', '$stateParams',  'ProductFactory', PhoneDetailCtrl]);
Application.Controllers.controller('FavoritesCtrl', ['$scope', 'ProductFactory', FavoritesCtrl]);
Application.Controllers.controller('CompareCtrl', ['$scope', 'ProductFactory', CompareCtrl]);
Application.Controllers.controller('tabsController', ['$scope', tabsController]);
Application.Controllers.controller('sharemodalCtrl', ['$scope', '$ionicModal', sharemodalCtrl]);
Application.Controllers.controller('additemCtrl', ['$scope', '$ionicModal', additemCtrl]);
Application.Controllers.controller('sidenavCtrl', ['$scope', '$ionicSideMenuDelegate', sidenavCtrl]);
Application.Controllers.controller('MainCtrl', ['$scope','$location', MainCtrl]);

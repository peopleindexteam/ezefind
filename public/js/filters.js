'use strict';

var checkmark = function() {
	return function(input) {
		return input ? '\u2713' : '\u2718';
	};
};

Application.Filters.filter('checkmark', checkmark);
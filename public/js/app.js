'use strict';

var Application = Application || {};

Application.Controllers = angular.module('application.controllers', ['application.filter']);
Application.Services = angular.module('application.services', []);
Application.Constants = angular.module('application.constants', []);
Application.Filters = angular.module('application.filter', []);

angular.module('application', ['ionic', 'application.controllers', 'application.filter', 'application.services'])

/*For Mobile*/
/*.run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		if(window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
	});
})
*/
.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('app', {
			url: '/app',
			abstract: true,
			templateUrl: 'templates/menu.html',
			controller: 'AppCtrl'
		})
		.state('app.search', {
			url: '/search',
			views: {
			'menuContent' : {
				templateUrl: 'templates/search.html'
				}
			}
		})
		.state('app.browse', {
			url: '/browse',
			views: {
			'menuContent' : {
				templateUrl: 'templates/browse.html'
				}
			}
		})
		.state('app.compare', {
			url: '/compare',
			views: {
			'menuContent' : {
				templateUrl: 'templates/compare.html',
				controller: 'CompareCtrl'
				}
			}
		})
		.state('app.favourites', {
			url: '/favourites',
			views: {
			'menuContent' : {
				templateUrl: 'templates/favourites.html',
				controller: 'FavoritesCtrl'
				}
			}
		})
		.state('app.homepage', {
			url: '/homepage',
			views: {
			'menuContent' : {
				templateUrl: 'templates/homepage.html',
				controller: 'AppCtrl'
				}
			}
		})
		.state('app.mobile-home', {
			url: '/mobile-home',
			views: {
			'menuContent' : {
				templateUrl: 'templates/mobile-home.html',
				}
			}
		})
		.state('app.mobile',{
			url: '/mobile',
			views: {
			'menuContent' : {
				templateUrl: 'templates/mobile.html'
				}
			}
		})
		.state('app.broadband',{
			url: '/broadband',
			views: {
			'menuContent' : {
				templateUrl: 'templates/broadband.html'
				}
			}
		})
		.state('app.starter-kits',{
			url: '/starter-kits',
			views: {
			'menuContent': {
				templateUrl: 'templates/starter-kits.html'
				}
			}
		})
		.state('app.recharge',{
			url: '/recharge',
			views: {
			'menuContent': {
				templateUrl: 'templates/recharge.html'
				}
			}
		})
		.state('app.product-detail', {
			url: '/:phoneId',
			views: {
			'menuContent' : {
				templateUrl: 'templates/tpl/product-detail.html',
				controller: 'PhoneDetailCtrl'
				}
			}
		})
		.state('app.single', {
			url: '/playlists/:playlistId',
			views: {
			'menuContent' : {
				templateUrl: 'templates/playlist.html',
				controller: 'PlaylistCtrl'
				}
			}
		});

		$urlRouterProvider.otherwise('/app/homepage');
});

